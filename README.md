# Powiększony nagłówek

### Paragrafy

To jest pierwszy paragraf.

To jest drugi paragraf.

To jest trzeci paragrag.

**pogrubienie** *kursywa* ~~przekreślenie~~

### Cytat

> Jeśli coś Ci w życiu nie idzie i wpadasz pyskiem w błoto to musisz być jak **dzik**: pchać to błoto ryjem do przodu ~ Mariusz Pudzianowski 

### Zagnieżdżona lista numeryczna

1. Pierwszy element
2. Drugi element
    1. Pierwszy podelement
    2. Drugi podelement
3. Trzeci element

### Zagnieżdżona lista nienumeryczna

- Pierwszy element
- Drugi element
    - Pierwszy podelement
    - Drugi podelement
- Trzeci element

### Blok kodu

```
def bubble_sort(lista):
    list=copy(lista)
    for i in range(len(list)):
        for j in range(len(list)-i-1):
            if list[j+1]<list[j]:
                list[j], list[j+1]=list[j+1], list[j]
    return list
```

### Kod programu w tekście 

Tekst Tekst Tekst Tekst Tekst Tekst Tekst Tekst Tekst
Tekst Tekst Tekst Tekst `kod programu` Tekst Tekst

### Obraz

![Pudzian](/pudzian.jpg)